package model;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class testUser {
    private User user;
    @Before
    public void setUp() throws Exception{

        user = new User("Max");

    }

    @Test(expected = Exception.class)
    public void testConstructorNull() throws Exception{
        User u = new User(null);
    }

    @Test
    public void testGetFirstname(){
        assertEquals("Max", user.getFirstname());

    }
    @Test
    public void testSetFirstname(){
        assertEquals(true, user.setFirstname("Test"));
        assertEquals("Test", user.getFirstname());
    }
    @Test

    public void testSetFirstnameNull() {

        assertEquals(false, user.setFirstname(null));
        assertNotEquals(null, user.getFirstname());
    }

    @Test
    public void testSetFirstnameEmpty() {
        assertEquals(false, user.setFirstname(""));
        assertNotEquals("",user.getFirstname());
    }

    @Test
    public void testSetFirstnameCharacterset() {
        user.setFirstname("äßöü");
        assertEquals("äßöü", user.getFirstname());
    }
}
