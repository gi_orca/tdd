package model;

public class User {
private String firstname;

   public User(String firstname) throws Exception{
       boolean success = true;
       success &= setFirstname(firstname);

       if (!success){
           throw new Exception("Empty name on constructor");
       }
   }
    public String getFirstname(){
        return this.firstname;
    }

    public boolean setFirstname(String name){
       if (name != null && !name.equals("")) {
           this.firstname = name;
           return true;
       }
       return false;
    }

}
